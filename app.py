from distutils.log import debug
from flask import Flask, jsonify, request
import flask_excel as excel
from flask_cors import CORS
import psycopg2.extras
import psycopg2
from loguru import logger
MEMBERS = [
    {
        'id': '0',
        'name': 'test',
        'area': 'test',
        'okved': 'test',
        'kol': '2',
        'pul': '3',
        'posting_date': '2022-05-03'
        
    }
    
]

 
# instantiate the app
app = Flask(__name__)
excel.init_excel(app)
app.config.from_object(__name__)
     
def get_db_connection():
    conn = psycopg2.connect(host='******',
                            database='****',
                            user='***',
                            password='*****',
                            port ='4194',
                            options="-c search_path=data,public"
                            )
    return conn
 
# enable CORS
CORS(app, resources={r'/*': {'origins': '*'}})
 
 
 
@app.route('/members', methods=['GET'])
def all_members():
    return jsonify({
        'status': 'success',
        'members': MEMBERS
    })
 
@app.route('/',methods=['GET', 'POST'])
def home():
    conn = get_db_connection()
    cursor = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)

    if request.method == 'GET':
        try:
            cursor.execute("SELECT id,name,area,okved,kol,pul,kol+pul as t, posting_date from test1 order by id")
            userslist = cursor.fetchall()
            return jsonify({
                'status': 'success',
                'members': userslist
            })
            
        except psycopg2.DatabaseError as e:
            logger.error(e)
            raise e
        finally:
            cursor.close() 
            conn.close()
    elif request.method == 'POST':
        post_data = request.get_json(silent=True)
        name = post_data.get('name')
        area = post_data.get('area')
        okved = post_data.get('okved')
        
        query = "SELECT * from test1 where (1=1)"
        if name.strip() != '':
            query += f" and name like '%{name.strip()}%'"
        if area.strip() != '':
            query += f" and area like '%{area.strip()}%'"
        if okved.strip() != '':
            query += f" and okved like '%{okved.strip()}%'" 
        
        query += " order by id" 
        try:
            cursor.execute(query)
            userslist = cursor.fetchall()
            return jsonify({
                'status': 'success',
                'members': userslist
            })
        except psycopg2.DatabaseError as e:
            logger.error(e)
            raise e
        finally:
            cursor.close() 
            conn.close()
 
@app.route('/insert', methods=['GET', 'POST'])
def insert():
    conn = get_db_connection()
    cursor = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
    response_object = {'status': 'success'}
    if request.method == 'POST':
        post_data = request.get_json(silent=True)
        name = post_data.get('name')
        area = post_data.get('area')
        okved = post_data.get('okved')
        kol = post_data.get('kol')
        pul = post_data.get('pul')
        print(name)
        print(area)
        print(okved)
        print(kol)
        print(pul)
        sql = "INSERT INTO test1(name,area,okved,kol,pul) VALUES(%s, %s, %s, %s, %s)"
        data = (name, area, okved, kol, pul)
        conn = get_db_connection()
        cursor = conn.cursor()
        cursor.execute(sql, data)
        conn.commit()
 
        response_object['message'] = "Успешно добавлено"
    return jsonify(response_object)
 
@app.route('/edit/<string:id>', methods=['GET', 'POST'])
def edit(id):
    conn = get_db_connection()
    cursor = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
    print(id)
    cursor.execute("SELECT * FROM test1 WHERE id = %s", [id])
    row = cursor.fetchone() 
 
    return jsonify({
        'status': 'success',
        'editmember': row
    })
 
@app.route('/update', methods=['GET', 'POST'])
def update():
    conn = get_db_connection()
    cursor = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
    response_object = {'status': 'success'}
    if request.method == 'POST':
        post_data = request.get_json(silent=True)
        edit_id = post_data.get('edit_id')
        edit_name = post_data.get('edit_name')
        edit_area = post_data.get('edit_area')
        edit_okved = post_data.get('edit_okved')
        edit_kol = post_data.get('edit_kol')
        edit_pul = post_data.get('edit_pul')
        print(edit_name)
        print(edit_area)
        print(edit_okved)
        print(edit_kol)
        print(edit_pul)
        cursor.execute ("UPDATE test1 SET name=%s, area=%s, okved=%s, kol=%s, pul=%s WHERE id=%s",(edit_name, edit_area, edit_okved, edit_kol, edit_pul,edit_id))
        conn.commit()
        cursor.close()
 
        response_object['message'] = "Успешно изменено"
    return jsonify(response_object)
 
@app.route('/delete/<string:id>', methods=['GET', 'POST'])
def delete(id):
    conn = get_db_connection()
    cursor = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
   
    response_object = {'status': 'success'}
 
    cursor.execute("DELETE FROM test1 WHERE id = %s", [id])
    conn.commit()
    cursor.close()
    response_object['message'] = "Запись удалена"
    return jsonify(response_object)
 

@app.route("/download", methods=['GET'])
def download_file():
    conn = get_db_connection()
    cursor = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
    try:
        cursor.execute("""SELECT name as Организация, area as Район, okved as ОКВЭД, kol as "Предпенсионного возраста (чел)", pul as "Под санкциями (чел)", kol+pul as Всего, posting_date as "Дата внесения в отчет" from test1 order by id""")
        userslist = cursor.fetchall()
        return excel.make_response_from_records(userslist, "xlsx")
    except psycopg2.DatabaseError as e:
        logger.error(e)
        raise e
    finally:
        cursor.close() 
        conn.close()

if __name__ == '__main__':
    app.run(debug = True)